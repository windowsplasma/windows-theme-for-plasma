This was made with Debian 10 and KDE Plasma 5.14.5 in mind.

**Prerequisites:**

- oxygen-icon-theme

- qt5-style-kvantum

- fonts-ubuntu* (optional of course, but I just think it looks good)





**Cursor theme:**

- https://store.kde.org/p/999972





**GTK Theme:**

- https://github.com/B00merang-Project/Windows-7





**Plasmoids:**

- Excalibur: https://www.pling.com/p/1172867

- Event Calendar: https://www.pling.com/p/998901





**Kwin Scripts:**

- Force Blur: https://store.kde.org/p/1294604/





***Recommended Dolphin Services:***

- Set as Wallpaper: https://www.pling.com/p/1169583

- Root Actions: https://www.pling.com/p/998469/





***Reccomended Wallpaper Plugins:***

- Smart Video Wallpaper: https://store.kde.org/p/1316299/





**Installation:**

- Copy the Aurorae, Kvantum, and Plasma desktop, and icon themes to their respective locations (read the "where does this go?" files if you're not sure where they belong)





**Additional Info:**

- Under Desktop Effects, disable background contrast and enable blur

- Blur strength should be 4 and noise strength should be off

- Use Oxygen as the fallback icon theme

- Set the panel to exactly 40px in height

- Replace the default clock with Event Calendar

- In the Event Calendar settings, enable lines 1 and 2, and set the fixed clock height to 31px (or whatever looks best with your preferred font)

- Replace the default launcher with Excalibur (also install kmenuedit to customize the launcher, if you wish)

- In the Excalibur settings, change the icon to KDESevenOrb.png





***Firefox theme:***

- Add "firefox" or "firefox-esr" to the Force Blur rules.

- Copy userChrome.css into the "chrome" directory under your Firefox profile. If the directory does not exist, create it. To enable this, go into about:config in Firefox and set gfx.webrender.all and toolkit.legacyUserProfileCustomizations.stylesheets to true.

- The Firefox theme is Aero Glass by 0oWow, with various Linux-specific modifications. 

- Original: https://old.reddit.com/r/FirefoxCSS/comments/7o8a6j/code_screenshot_aero_glass_windows_10_firefox_57/





**Credits:**

- The aurorae theme is based on Seven Black:
https://store.kde.org/p/1002615

- The Kvantum theme is based on Kv Glass:
https://store.kde.org/p/1201321

- The desktop theme is based on Ronak and Seven Black:
https://store.kde.org/p/998787 and https://www.pling.com/p/998614

- The icon theme is based on Vista icon theme by hermes37:
https://www.deviantart.com/hermes37/art/Vista-icon-theme-148921512


